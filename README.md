### Why use this loader
- use img like in html 
- imgSource(options) make url simplify 
- loader help you require img. but @ must be use;
### Install
npm install jsx-img-loader --save-dev

or

yarn add jsx-img-loader --save-dev

### Usage
```
{
	test: /\.js$/,
	use: ['babel-loader',{
		loader: 'jsx-img-loader',
		options:{
			imgSource: './static/img'
		}
	}],
	exclude: /node_modules/,
	include: path.resolve(__dirname,'src')
},
```
### In jsx 
```
<img src="@1.png" alt=""/>
```
or

```
const imgUrl = "@1.png"

...

<img src={imgUrl} alt=""/>
```

### Options

imgSource:   Specifies that resource paths can be relative and absolute.so you can write less